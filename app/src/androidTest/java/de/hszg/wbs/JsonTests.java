package de.hszg.wbs;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class JsonTests {
    @Test
    public void test_storing_and_loading_profile() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        UserProfile inputProfile = new UserProfile(
                "Alice",
                12,
                2,
                3,
                "#FFFF00",
                new ArrayList<Integer>(),
                0
        );
        JsonUtil.WBSProfileToJson(appContext, inputProfile);
        UserProfile outputProfile = JsonUtil.readProfileFromJson(appContext);
        assertEquals(inputProfile.getName(), outputProfile.getName());
        assertEquals(inputProfile.getAge(), outputProfile.getAge());
        assertEquals(inputProfile.getFollower(), outputProfile.getFollower());
        assertEquals(inputProfile.getStars(), outputProfile.getStars());
        assertEquals(inputProfile.getColor(), outputProfile.getColor());
        assertEquals(inputProfile.getQuestionIds(), outputProfile.getQuestionIds());
        assertEquals(inputProfile.getLocalMap(), outputProfile.getLocalMap());
    }

    @After
    public void clean_up(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        JsonUtil.DeleteProfile(appContext);
    }
}
