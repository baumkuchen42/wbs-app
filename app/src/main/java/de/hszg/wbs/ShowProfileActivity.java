package de.hszg.wbs;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ShowProfileActivity extends AppCompatActivity {
    private UserProfile wbsProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wbsProfile = JsonUtil.readProfileFromJson(this);
        if (getIntent().getBooleanExtra("existingProfileEdited", false)) {
            Notifications.displayToast(this, R.string.profileEditSuccessMsg);
        }
        Utilities.loadTheme(this, wbsProfile.getColor());
        setContentView(R.layout.activity_show_profile);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefillValues();
    }

    private void prefillValues() {
        final TextView TxtName = findViewById(R.id.SPA_text_name2);
        TxtName.setText(wbsProfile.getName());
        final TextView TxtAge = findViewById(R.id.SPA_text_age2);
        TxtAge.setText(String.valueOf(wbsProfile.getAge()));
        final TextView TxtColor = findViewById(R.id.SPA_text_color2);
        TxtColor.setText(wbsProfile.getColor());
        TxtColor.setTextColor(Color.parseColor(wbsProfile.getColor()));
        TxtColor.setBackgroundColor(Color.parseColor(wbsProfile.getColor()));

        final TextView TxtStars = findViewById(R.id.SPA_text_starcount);
        TxtStars.setText(wbsProfile.getStars() + "x");

        final ImageView ImageFollower = findViewById(R.id.SPA_image_follower);
        ImageFollower.setImageResource(wbsProfile.getFollower());
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    public void onBackPressed(){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(myIntent);
        finish();
    }

    public void onEditImageClicked(View view) {
        Intent intent = new Intent(getApplicationContext(), CreateProfileActivity.class);
        intent.putExtra("editingExistingProfile", true);
        startActivity(intent);
        finish();
    }

    public void onDeleteImageClicked(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(ShowProfileActivity.this);
        alert.setTitle("WBS - Profile");
        alert.setMessage(R.string.profileDeleteMsg);
        alert.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteProfileAndLaunchCreateProfileActivity();
            }
        });
        alert.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.create();
        alert.show();
    }

    private void deleteProfileAndLaunchCreateProfileActivity() {
        JsonUtil.DeleteProfile(ShowProfileActivity.this);
        startActivity(new Intent(getApplicationContext(), CreateProfileActivity.class));
        finish();
    }
}
