package de.hszg.wbs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashScreen extends AppCompatActivity {

    private UserProfile wbsProfile;

    private static final int SPLASH_TIME = 2000; //2 seconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        wbsProfile = JsonUtil.readProfileFromJson(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startMainActivityOrCreateProfile();
            }
        }, SPLASH_TIME);
    }

    private void startMainActivityOrCreateProfile() {
        Intent myIntent;
        if(wbsProfile != null) {
            myIntent = new Intent(getApplicationContext(), MainActivity.class);
            myIntent.putExtra("from","splash");
        } else {
            myIntent = new Intent(getApplicationContext(), CreateProfileActivity.class);
        }
        startActivity(myIntent);

        finish();
    }
}
