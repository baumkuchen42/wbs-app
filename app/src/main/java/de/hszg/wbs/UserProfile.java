package de.hszg.wbs;

import java.util.ArrayList;

public class UserProfile {

    private String name = "";
    private int age = 0;
    private int follower = 0;
    private String color = "#FFFFFF";
    private int stars = 0;
    private ArrayList<Integer> questionIds = new ArrayList<>();
    private int localMap = 0;
    private boolean defaultProfile = true;

    public static UserProfile createDefaultProfile() {
        return new UserProfile();
    }

    private UserProfile() {}

    UserProfile(String name, int age, int follower, int stars, String color, ArrayList<Integer> questionIds, int localMap) {
        this.name = name;
        this.age = age;
        this.follower = follower;
        this.stars = stars;
        this.color = color;
        this.questionIds = questionIds;
        this.localMap = localMap;
        this.defaultProfile = false;
    }

    @Override
    public String toString() {
        return "name: " + this.name + " gender: " + " age: " + this.age +
                " follower: " + this.follower + " stars: " + this.stars + " color: " + this.color +
                " questionIds: " + this.questionIds + " localMap: " + this.localMap +
                " defaultProfile: " + this.defaultProfile;
    }

    public boolean isDefaultProfile() {
        return this.defaultProfile;
    }

    public void setDefaultProfile(boolean isDefault) {
        defaultProfile = isDefault;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getFollower() {
        return follower;
    }

    public void setFollower(int follower) {
        this.follower = follower;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public void incrementStars() {
        this.stars++;
    }

    public void decrementStars() {
        this.stars--;
    }

    public int getStars() {
        return stars;
    }

    public ArrayList<Integer> getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(ArrayList<Integer> questionId) {
        this.questionIds = questionId;
    }

    public void addQuestionId(int questionId) {
        if(!checkQuestionId(questionId)) {
            this.questionIds.add(questionId);
            this.incrementStars();
        }
    }

    public boolean checkQuestionId(int questionId) {
        for (int question : this.questionIds) {
            if(question == questionId) {
                return true;
            }
        }
        return false;
    }

    public void deleteAllQuestions() {
        this.questionIds.clear();
    }

    public int getLocalMap() {
        return localMap;
    }

    public void setLocalMap(int localMap) {
        this.localMap = localMap;
    }
}
