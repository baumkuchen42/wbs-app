package de.hszg.wbs;

public class Video {
    private int id;
    private String name;
    private String description;
    private String filepath;
    private int questionId;

    Video(int id, String name, String description, String filepath, int questionId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.filepath = filepath;
        this.questionId = questionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
}
