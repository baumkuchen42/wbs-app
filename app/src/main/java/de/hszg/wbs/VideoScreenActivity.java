package de.hszg.wbs;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.VideoView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class VideoScreenActivity extends AppCompatActivity {

    private VideoView videoView;
    private boolean started=false;
    private ImageButton playButton, pauseButton, repeatButton;
    private int videoNumber;
    private String videoPath ="";
    private int videoQuestionId = -1;
    private Animation animAlpha;
    private boolean isLocalVideo;

    private UserProfile wbsProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadConfiguration();
        Utilities.loadTheme(this, wbsProfile.getColor());
        setContentView(R.layout.activity_video_screen);
        loadViews();
        configureVideoView();
    }

    private void loadConfiguration() {
        this.wbsProfile = JsonUtil.readProfileFromJson(this);
        videoNumber = (int) getIntent().getExtras().getSerializable("videoNumber");
        ArrayList videos = JsonUtil.readVideosFromJson(this);
        for(int i=0; i<videos.size(); i++){

            Video video = (Video) videos.get(i);
            if(video.getId() == videoNumber){
                videoPath = video.getFilepath();
                videoQuestionId = video.getQuestionId();
                break;
            }
        }
        animAlpha = AnimationUtils.loadAnimation(this,R.anim.anim_alpha);
        if (videoPath.contains("res/raw")) {
            isLocalVideo = false;
            videoPath = videoPath.split("/")[2].split("\\.")[0];
            Log.d("videoo", videoPath);
        }
        else {
            isLocalVideo = true;
            Log.d("videoo", videoPath);
        }
    }

    private void loadViews() {
        playButton = findViewById(R.id.playButtonID);
        pauseButton = findViewById(R.id.pauseButtonID);
        repeatButton = findViewById(R.id.repeatButtonID);
    }

    private void configureVideoView() {
        videoView = findViewById(R.id.videoViewID);
        if (isLocalVideo) {
            videoView.setVideoPath(videoPath);
        }
        else {
            String fullPath = "android.resource://" + getPackageName() + "/" + getResources().getIdentifier(videoPath,"raw",""+getPackageName());
            Log.d("videoo", fullPath);
            videoView.setVideoPath(fullPath);
        }
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                onVideoCompletion();
            }
        });
    }

    public void onVideoCompletion() {
        repeatButton.setVisibility(View.VISIBLE);
        started=false;
    }

    public void onBackPressed(){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(myIntent);
        finish();
    }

    public void onContinueButtonClicked(View view) {
        Intent myIntent = new Intent(getApplicationContext(), QuestionActivity.class);;
        myIntent.putExtra("videoQuestionId", videoQuestionId);
        myIntent.putExtra("videoNumber", videoNumber);
        startActivity(myIntent);
        finish();
    }

    public void onRepeatButtonClicked(View view) {
        repeatButton.setVisibility(View.GONE);
        started = true;
        videoView.start();
    }

    public void onBackButtonClicked(View view) {
        onBackPressed();
    }

    public void onPlayButtonClicked(View view) {
        if (!started) {
            playButton.setVisibility(View.GONE);
            started = true;
            videoView.start();
        } else {
            if (!videoView.isPlaying()) {
                pauseButton.clearAnimation();
                pauseButton.setVisibility(View.GONE);
                videoView.start();
                view.setVisibility(View.VISIBLE);
                view.startAnimation(animAlpha);
                view.setVisibility(View.GONE);

            } else {
                view.clearAnimation();
                view.setVisibility(View.GONE);
                videoView.pause();
                pauseButton.setVisibility(View.VISIBLE);
                pauseButton.startAnimation(animAlpha);
                pauseButton.setVisibility(View.GONE);
            }
        }
    }

    public void onPauseButtonClicked(View view) {
        videoView.start();
        pauseButton.clearAnimation();
        pauseButton.setVisibility(View.GONE);
        started=true;
        playButton.setVisibility(View.VISIBLE);
        playButton.startAnimation(animAlpha);
        playButton.setVisibility(View.GONE);
    }

    public void onVideoViewClicked(View view) {
        if(started) {
            if (videoView.isPlaying()) {
                playButton.clearAnimation();
                videoView.pause();
                pauseButton.setVisibility(View.VISIBLE);
                pauseButton.startAnimation(animAlpha);
                pauseButton.setVisibility(View.GONE);
            } else {
                pauseButton.clearAnimation();
                videoView.start();
                playButton.setVisibility(View.VISIBLE);
                playButton.startAnimation(animAlpha);
                playButton.setVisibility(View.GONE);
            }
        }
    }
}

