package de.hszg.wbs;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.PathUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class AddQuestionActivity extends AppCompatActivity {
    private UserProfile wbsProfile;
    private ArrayList<World> worlds;

    private static final int FILE_SELECT_CODE = 0;
    private Spinner worldChooser;

    private String videoPath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadConfiguration();
        loadGraphics();
    }

    private void loadConfiguration() {
        wbsProfile = JsonUtil.readProfileFromJson(this);
        worlds = JsonUtil.readWorldsFromJson(this);
    }

    private void loadGraphics() {
        Utilities.loadTheme(this, wbsProfile.getColor());
        setContentView(R.layout.activity_add_question);
        fillWorldChooser();
    }

    private void fillWorldChooser() {
        worldChooser = findViewById(R.id.worldChooser);
        List<String> list = new ArrayList<>();
        list.add(getResources().getString(R.string.addQuestion_worldChooserHint));
        for(World world : this.worlds) {
            if(!world.isFull())
                list.add(world.getName());
            Log.d("worldfull", world.getWayPoints().size() + " " + MainActivity.MAX_NR_WAYPOINTS);
        }
        ArrayAdapter<String> dataAdapter = new CostumSpinnerAdapter(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        worldChooser.setAdapter(dataAdapter);
    }

    public void showFileChooser(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("video/mp4");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            if(Utilities.isPermissionGrantedForStorage(this)) {
                startActivityForResult(
                        Intent.createChooser(intent, "Select a File"),
                        FILE_SELECT_CODE);
            }
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
            Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    try {
                        this.videoPath = Utilities.getPathFromUri(this, uri);
                        String newVideoPath = getFilesDir()+"/"+Utilities.getNameFromPath(videoPath);
                        Utilities.copyFile(new File(videoPath), new File(newVideoPath));
                        this.videoPath = newVideoPath;
                        Button videoButton = findViewById(R.id.videoChooserButton);
                        videoButton.setText(Utilities.getNameFromPath(videoPath));
                    }
                    catch(Exception e) { // an IllegalArgumentException occurs with newer version of android where URIs are not always data
                        try { // thus we try to create a stream from the uri and copy that one to a new file
                            String newVideoPath = getFilesDir()+"/"+JsonUtil.getNrOfVideos(this)+".mp4"; // new name is just the consecutive video ID
                            Utilities.createFileFromStream(getContentResolver().openInputStream(uri), new File(newVideoPath));
                            this.videoPath = newVideoPath;
                            Button videoButton = findViewById(R.id.videoChooserButton);
                            videoButton.setText(Utilities.getNameFromPath(videoPath));
                        } catch (FileNotFoundException ex) {
                            ex.printStackTrace();
                            Notifications.displayToast(this, R.string.didntWorkMsg);
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onFinishButtonClicked(View view) {
        EditText question = findViewById(R.id.questionEdit);
        ArrayList<String> answers = getAnswersFromEdits();
        int rightAnswer = getRightAnswerFromCheckboxes();
        String worldName = (String) this.worldChooser.getSelectedItem();
        if (this.videoPath != null
                && !Utilities.isEmpty(question)
                && !answers.isEmpty()
                && rightAnswer != -1
                && worldName != getResources().getString(R.string.addQuestion_worldChooserHint)) {
            JsonUtil.addNewQuestion(this, this.videoPath, question.getText().toString(), answers, rightAnswer, worldName);
            displaySuccessAlert();
        }
        else {
            Log.d("addQuestion", videoPath + " " + question.getText() + " " + answers + " " + rightAnswer + " " + worldName);
            Notifications.displayAlert(this, R.string.app_name, R.string.addQuestion_fail);
        }
    }

    public void displaySuccessAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.app_name);
        alert.setMessage(R.string.addQuestion_success);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                launchMainActivity();
            }
        });
        alert.create();
        alert.show();
    }

    private ArrayList<String> getAnswersFromEdits() {
        EditText[] answerEdits = new EditText[]{
                findViewById(R.id.answer1),
                findViewById(R.id.answer2),
                findViewById(R.id.answer3),
                findViewById(R.id.answer4)};
        ArrayList<String> answers = new ArrayList<>();
        for(EditText answerEdit : answerEdits) {
            if (!Utilities.isEmpty(answerEdit)) {
                answers.add(answerEdit.getText().toString());
            }
        }
        return answers;
    }

    private int getRightAnswerFromCheckboxes() {
        CheckBox[] checkBoxes = new CheckBox[]{
                findViewById(R.id.checkBox1),
                findViewById(R.id.checkBox2),
                findViewById(R.id.checkBox3),
                findViewById(R.id.checkBox4),
        };
        for(CheckBox check : checkBoxes) {
            if (check.isChecked()) {
                return Integer.valueOf(check.getTag().toString());
            }
        }
        return -1;
    }

    private void launchMainActivity() {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(myIntent);
        finish();
    }

    private class CostumSpinnerAdapter extends ArrayAdapter<String> {

        public CostumSpinnerAdapter(@NonNull Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        @Override
        public boolean isEnabled(int position) {
            if (position == 0) {
                // Disable the first item from Spinner
                // First item will be use for hint
                return false;
            } else {
                return true;
            }
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view = super.getDropDownView(position, convertView, parent);
            TextView tv = (TextView) view;
            if(position == 0){
                // Set the hint text color gray
                tv.setTextColor(Color.GRAY);
            }
            else {
                tv.setTextColor(Color.BLACK);
            }
            return view;
        }
    }

}
