package de.hszg.wbs;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class ToAddQuestionActivity extends AppCompatActivity {
    private UserProfile wbsProfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wbsProfile = JsonUtil.readProfileFromJson(this);
        Utilities.loadTheme(this, wbsProfile.getColor());
        setContentView(R.layout.activity_to_add_question);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addFollowerIconToButton();
    }

    private void addFollowerIconToButton() {
        Button btn = findViewById(R.id.backToMain);
        Drawable follower = getResources().getDrawable(wbsProfile.getFollower());
        btn.setCompoundDrawablesWithIntrinsicBounds(null, null, follower, null);
    }

    public void launchMainActivity(View view) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(myIntent);
        finish();
    }

    public void onBackPressed(){
        launchMainActivity(null);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        launchMainActivity(null);
        return true;
    }

    public void launchAddQuestionActivity(View view) {
        Intent myIntent = new Intent(getApplicationContext(), AddQuestionActivity.class);
        startActivity(myIntent);
        finish();
    }
}
