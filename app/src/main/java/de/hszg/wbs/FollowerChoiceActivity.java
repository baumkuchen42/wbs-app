package de.hszg.wbs;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class FollowerChoiceActivity extends AppCompatActivity {

    private static final String active_follower = "#FFFFFF";
    private static final String non_active_follower = "#000000";
    private UserProfile wbsProfile;

    private Button nextButton;

    private static final int[] FOLLOWER_BUTTON_IDS = {
            R.id.FCA_ImageButton_follower_1,
            R.id.FCA_ImageButton_follower_2,
            R.id.FCA_ImageButton_follower_3,
            R.id.FCA_ImageButton_follower_4,
            R.id.FCA_ImageButton_follower_5,
            R.id.FCA_ImageButton_follower_6,
            R.id.FCA_ImageButton_follower_7,
            R.id.FCA_ImageButton_follower_8,
            R.id.FCA_ImageButton_follower_9,
            R.id.FCA_ImageButton_follower_10,
            R.id.FCA_ImageButton_follower_11,
            R.id.FCA_ImageButton_follower_12
    };
    private static final int[] FOLLOWER_RESOURCE_IDS = {
            R.drawable.icon_follower_1,
            R.drawable.icon_follower_2,
            R.drawable.icon_follower_3,
            R.drawable.icon_follower_4,
            R.drawable.icon_follower_5,
            R.drawable.icon_follower_6,
            R.drawable.icon_follower_7,
            R.drawable.icon_follower_8,
            R.drawable.icon_follower_9,
            R.drawable.icon_follower_10,
            R.drawable.icon_follower_11,
            R.drawable.icon_follower_12
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        wbsProfile = JsonUtil.readProfileFromJson(this);
        Utilities.loadTheme(this, wbsProfile.getColor());
        setContentView(R.layout.activity_follower_choice);

        //NEXT BUTTON
        nextButton = findViewById(R.id.FCA_button_next);
        nextButton.setEnabled(false);

        setFollowerFromProfile();
    }

    /**
     * If the activity was started to edit an existing profile, open the ShowProfileActivity.
     * If that isn't the case (when creating a completely new profile), open the EnterNewWorldActivity.
     */
    public void onNextButtonClicked(View view) {
        Intent intent;
        if (getIntent().getBooleanExtra("editingExistingProfile", false)) {
            intent = new Intent(getApplicationContext(), ShowProfileActivity.class);
            intent.putExtra("existingProfileEdited", true);
        } else {
            intent = new Intent(getApplicationContext(), EnterNewWorldActivity.class);
        }
        JsonUtil.WBSProfileToJson(FollowerChoiceActivity.this, wbsProfile);
        startActivity(intent);
        finish();
    }

    public void onBackButtonClicked(View view) {
        JsonUtil.WBSProfileToJson(FollowerChoiceActivity.this, wbsProfile);
        startActivity(new Intent(getApplicationContext(), CreateProfileActivity.class));
        finish();
    }

    public void onFollowerButtonClicked(View view) {
        setFollowerButtonHighlighted(view);
        nextButton.setEnabled(true);
        wbsProfile.setFollower(FOLLOWER_RESOURCE_IDS[Integer.valueOf(view.getTag().toString()) - 1]);
    }

    private void setFollowerFromProfile() {
        for(int i = 0; i<FOLLOWER_RESOURCE_IDS.length; i++) {
            if(wbsProfile.getFollower() == FOLLOWER_RESOURCE_IDS[i]) {
                setFollowerButtonHighlighted(findViewById(FOLLOWER_BUTTON_IDS[i]));
                nextButton.setEnabled(true);
            }
        }
    }

    private void setFollowerButtonHighlighted(View view) {
        clearHighlightedFollowerButtons();
        view.setBackgroundColor(Color.parseColor(active_follower));
    }

    private void clearHighlightedFollowerButtons() {
        for(int id : FOLLOWER_BUTTON_IDS) {
            findViewById(id).setBackgroundColor(Color.parseColor(non_active_follower));
        }
    }
}
