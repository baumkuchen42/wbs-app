package de.hszg.wbs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
	private UserProfile wbsProfile;
	private ArrayList<Video> videos;
	private int worldId;
	private ActionBarDrawerToggle mToggle;
	public static final int EUROPE = 0;
	public static final int AMERICA = 1;
	public static int MAX_NR_WAYPOINTS;
	private World world;
	private ArrayList<World> worlds;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		loadConfiguration();
		Utilities.loadTheme(this, wbsProfile.getColor());
		setContentView(R.layout.activity_main);
		MAX_NR_WAYPOINTS = getMaxNumberWayPoints();
		if(getIntent().getStringExtra("from") != null && getIntent().getStringExtra("from").equals("splash"))
			Notifications.welcomeNotification(this, wbsProfile.getName(), wbsProfile.getFollower());
		initializeMenu();
		initializeQuestionButtons();
		initializePrevWorldBtn();
		initializeNextWorldBtn();
	}

	private void loadConfiguration() {
		wbsProfile = JsonUtil.readProfileFromJson(this);
		if(wbsProfile.isDefaultProfile()) {
			launchCreateProfileActivity();
		}
		videos = JsonUtil.readVideosFromJson(this);
		this.worlds = JsonUtil.readWorldsFromJson(this);
		worldId = wbsProfile.getLocalMap();
		if(worldId >= this.worlds.size()) {
			worldId = this.worlds.size() - 1;
		}
		this.world = this.worlds.get(worldId);
	}

	private void initializeMenu() {
		DrawerLayout mDrawerLayout = findViewById(R.id.menuLayout);
		mToggle = new ActionBarDrawerToggle(this, mDrawerLayout,R.string.open,R.string.close);
		mDrawerLayout.addDrawerListener(mToggle);
		mToggle.syncState();
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	private void initializeQuestionButtons() {
		final ArrayList<ArrayList<String>> wayPoints = world.getWayPoints();
		for(int i = 0; i < wayPoints.size(); i++) {
			int resourceName = getResources().getIdentifier("MA_imageView_" + i, "id", this.getPackageName());
			ImageView questionImg = findViewById(resourceName);
			final int videoId = Integer.parseInt(wayPoints.get(i).get(1));
			if(wbsProfile.checkQuestionId(videos.get(videoId).getQuestionId())) {
				questionImg.setImageResource(android.R.drawable.star_big_on);
			}
			questionImg.setVisibility(View.VISIBLE);
			questionImg.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					MainActivity.this.onQuestionImgClicked(v, videoId);
				}
			});
		}
	}

	private void initializePrevWorldBtn() {
		ImageView continentButtonPrev = findViewById(R.id.MA_button_prev);
		final int prevMap = worldId - 1;
		if (prevMap < 0) {
			continentButtonPrev.setVisibility(View.INVISIBLE);
		} else  {
			continentButtonPrev.setVisibility(View.VISIBLE);
			continentButtonPrev.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					switchWorld(prevMap);
				}
			});
		}
	}

	private void initializeNextWorldBtn() {
		ImageView continentButtonNext = findViewById(R.id.MA_button_next);
		final int nextMap = worldId + 1;
		if (nextMap >= worlds.size()) {
			continentButtonNext.setVisibility(View.INVISIBLE);
		} else {
			continentButtonNext.setVisibility(View.VISIBLE);
			continentButtonNext.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					switchWorld(nextMap);
				}
			});
		}
	}

	private void switchWorld(int worldId) {
		Intent myIntent = new Intent(getApplicationContext(), EnterNewWorldActivity.class);
		wbsProfile.setLocalMap(worldId);
		JsonUtil.WBSProfileToJson(getApplicationContext(), wbsProfile);
		startActivity(myIntent);
		finish();
	}

	private void onQuestionImgClicked(View questionImg, int videoId) {
		Intent myIntent = new Intent(getApplicationContext(), VideoScreenActivity.class);
		myIntent.putExtra("videoNumber", videoId);
		startActivity(myIntent);
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(mToggle.onOptionsItemSelected(item)){
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void openSettingsMenu(MenuItem item) {
		Notifications.displayAlert(this, "WBS", "Hier soll ein Menü hin, wo man z.B. Hintegrundmusik einstellen und muten kann.");
	}

	public void launchShowProfileActivity(MenuItem item) {
		startActivity(new Intent(getApplicationContext(), ShowProfileActivity.class));
		finish();
	}

	public void launchToAddQuestionActivity(MenuItem item) {
		startActivity(new Intent(getApplicationContext(), ToAddQuestionActivity.class));
	}

	private void launchCreateProfileActivity() {
		startActivity(new Intent(getApplicationContext(), CreateProfileActivity.class));
		finish();
	}

	public int getMaxNumberWayPoints() {
		int nrWayPoints = 0;
		ViewGroup constraintLayout = findViewById(R.id.constraintLayout);
		for(int i = 0; i < constraintLayout.getChildCount(); i++) {
			if(constraintLayout.getChildAt(i).getTag() != null
				&& constraintLayout.getChildAt(i).getTag().equals("wayPoint")) {
					nrWayPoints++;
			}
		}
		return nrWayPoints;
	}
}
