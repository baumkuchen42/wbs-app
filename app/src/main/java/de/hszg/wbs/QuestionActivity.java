package de.hszg.wbs;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class QuestionActivity extends AppCompatActivity {

    private UserProfile wbsProfile;
    private int chosenCheckBox;
    private final ArrayList<CheckBox> checkboxes = new ArrayList<>();
    private ArrayList<Question> questions;
    private int currentQuestionNr;

    private Button nextButton;
    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        wbsProfile = JsonUtil.readProfileFromJson(this);
        Utilities.loadTheme(this, wbsProfile.getColor());
        setContentView(R.layout.activity_question);

        this.nextButton = findViewById(R.id.QA_button_next);
        this.backButton = findViewById(R.id.QA_button_back);
        nextButton.setEnabled(false);

        this.questions = JsonUtil.readQuestionsFromJson(this);
        currentQuestionNr = (int) getIntent().getExtras().getSerializable("videoQuestionId");
        assembleShownQuestion();
    }

    public void onNextButtonClicked(View view) {
        AlertDialog.Builder alert;
        if(chosenCheckBox == questions.get(currentQuestionNr).getRightAnswer()) {
            alert = rightAnswerAlert();
        } else {
            alert = wrongAnswerAlert();
        }
        alert.create();
        alert.show();
    }

    public void onBackButtonClicked(View view){
        Intent myIntent = new Intent(getApplicationContext(), VideoScreenActivity.class);
        myIntent.putExtra("videoNumber", (int) getIntent().getExtras().getSerializable("videoNumber"));
        startActivity(myIntent);
        finish();
    }

    /**
     * So that pressing back in the navigation bar will also bring the user back to the video.
     */
    public void onBackPressed(){
        onBackButtonClicked(new View(this));
    }

    private void assembleShownQuestion() {
        final TextView TxtHeader = findViewById(R.id.QA_textView_header);
        TxtHeader.setText(questions.get(currentQuestionNr).getQuestion());

        final ImageView ImageFollower = findViewById(R.id.SPA_image_follower);
        ImageFollower.setImageResource(wbsProfile.getFollower());

        for (int i = 0; i < questions.get(currentQuestionNr).getNumberOfAnswers(); i++) {
            CheckBox check = createNewAnswerCheckBox(i);
            checkboxes.add(check);
            LinearLayout lin = findViewById(R.id.QA_LinearLayOut_answerLayout);
            lin.addView(check);
        }
    }

    private CheckBox createNewAnswerCheckBox(final int number) {
        CheckBox check = new CheckBox(this);
        check.setText(questions.get(currentQuestionNr).getAnswers().get(number));
        check.setPadding(10, 10, 10, 10);
        check.setTextSize(22);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(5, 5, 5, 5);
        params.gravity = Gravity.NO_GRAVITY;
        check.setLayoutParams(params);
        check.setGravity(Gravity.CENTER);
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCheckGroup((CheckBox) v);
                nextButton.setEnabled(true);
                chosenCheckBox = number;
            }

        });
        return check;
    }

    private void setCheckGroup(CheckBox cks) {
        for (int i = 0; i < checkboxes.size(); i++) {
            checkboxes.get(i).setChecked(false);
        }
        cks.setChecked(true);
    }

    private AlertDialog.Builder rightAnswerAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(QuestionActivity.this);
        alert.setTitle("WBS");
        alert.setIcon(wbsProfile.getFollower());
        alert.setMessage(R.string.rightAnswerCongrats);
        alert.setPositiveButton(R.string.forward, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                wbsProfile.addQuestionId(currentQuestionNr);
                JsonUtil.WBSProfileToJson(QuestionActivity.this, wbsProfile);

                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(myIntent);
                finish();
            }
        });
        return alert;
    }

    private AlertDialog.Builder wrongAnswerAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(QuestionActivity.this);
        alert.setTitle("WBS");
        alert.setIcon(wbsProfile.getFollower());
        alert.setMessage(R.string.wrongAnswerMsg);
        alert.setPositiveButton(R.string.newTry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.setNegativeButton(R.string.laterTry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(myIntent);
                finish();
            }
        });
        return alert;
    }
}
