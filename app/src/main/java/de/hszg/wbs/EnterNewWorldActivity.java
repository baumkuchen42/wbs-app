package de.hszg.wbs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class EnterNewWorldActivity extends AppCompatActivity {

    private UserProfile wbsProfile;
    private static int SPLASH_TIME = 2000; //2 seconds
    private World world;

    /**
     * Loads configuration (world and user profile),
     * loads all graphical elements of the activity
     * and creates a transition to the main activity.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadConfiguration();
        loadGraphics();
        setTransitionToMainActivity();
    }

    /**
     * Helper method to load configuration consisting of current wbsProfile and current world.
     */
    private void loadConfiguration() {
        wbsProfile = JsonUtil.readProfileFromJson(this);
        final int worldId = wbsProfile.getLocalMap();
        this.world = JsonUtil.readWorldsFromJson(this).get(worldId);
        Utilities.loadTheme(this, wbsProfile.getColor());
    }

    /**
     * Helper method to load and set all graphical elements of the enter world transition.
     */
    private void loadGraphics() {
        setContentView(R.layout.activity_enter_new_world);
        ImageView mapImageView = findViewById(R.id.ENWA_imageview_background);
        mapImageView.setContentDescription(getResources().getString(getResources().getIdentifier(world.getName(), "string", getPackageName())) + getResources().getString(R.string.ENWA_Map));
        TextView mapText = findViewById(R.id.ENWA_text_head);
        mapText.setText(getResources().getString(R.string.ENWA_Header) + " " + getResources().getString(getResources().getIdentifier(world.getName(), "string", getPackageName())));
        mapImageView.setImageResource(getResources().getIdentifier(world.getMap(), "drawable", this.getPackageName()));
    }

    /**
     * Helper method to activate the actual transition to the main activity.
     */
    private void setTransitionToMainActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(myIntent);
                finish();
            }
        }, SPLASH_TIME);
    }
}
