package de.hszg.wbs;

import java.util.ArrayList;

public class World {

    private final int id;
    private final String name;
    private String map;
    private String roadMap;
    private ArrayList<ArrayList<String>> wayPoints; // determines, which question icons get shown with which videos

    World(int id, String name, String map, String roadMap, ArrayList<ArrayList<String>> wayPoints) {
            this.id = id;
            this.name = name;
            this.map = map;
            this.roadMap = roadMap;
            this.wayPoints = wayPoints;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMap() { return map; }

    public String getRoadMap() { return roadMap; }

    public ArrayList<ArrayList<String>> getWayPoints() {
        return wayPoints;
    }

    public boolean isFull() {
        return this.wayPoints.size() == MainActivity.MAX_NR_WAYPOINTS;
    }

}


