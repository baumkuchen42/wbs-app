package de.hszg.wbs;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;

public class JsonUtil {
    private static final String PROFILE_FILE_NAME = "wbs_profile.json";
    private static final String VIDEO_FILE_NAME = "wbs_videos.json";
    private static final String QUESTION_FILE_NAME = "wbs_questions.json";
    private static final String WORLDS_FILE_NAME = "wbs_worlds.json";

    /**
     * Deletes the current wbs profile.
     * @param context the application context
     */
    public static void DeleteProfile(Context context) {
        File file = new File(context.getFilesDir(), PROFILE_FILE_NAME);
        file.delete();
    }

    /**
     * Reads the user profile from the json file which contains it
     * @param context the application context
     * @return a newly created user profile, either filled with values from the file or default if there's no profile file
     */
    public static UserProfile readProfileFromJson(Context context) {
        JSONObject jsonObj = null;
        UserProfile userProfile;
        try {
            jsonObj = new JSONObject(readFromJsonFile(context, PROFILE_FILE_NAME, true));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonObj != null){
            userProfile = fillProfileWithValuesFromJsonFile(jsonObj);
        }
        else {
            Log.d("readProfileFromJson", "creating default profile");
            userProfile = UserProfile.createDefaultProfile();
        }
        return userProfile;
    }

    /**
     * Saves a user profile to profile file.
     * @param context the application context
     * @param userProfile the user profile to be saved
     */
    public static void WBSProfileToJson(Context context, UserProfile userProfile) {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("name", userProfile.getName());
            jsonObj.put("age", userProfile.getAge());
            jsonObj.put("follower", userProfile.getFollower());
            jsonObj.put("color", userProfile.getColor());
            jsonObj.put("stars", userProfile.getStars());
            jsonObj.put("questionIds", userProfile.getQuestionIds().toString().replace("[","").replace("]","").replace(" ", ""));
            jsonObj.put("localMap", userProfile.getLocalMap());
        } catch (Exception e) {
            e.printStackTrace();
        }
        writeJsonToFile(jsonObj, context.getFilesDir()+"/"+PROFILE_FILE_NAME);
    }

    /**
     * Extracts information of the videos from video file and puts it in an array list with video objects.
     * @param context the application context
     * @return array list with video objects, each video object containing all needed video information
     */
    public static ArrayList<Video> readVideosFromJson(Context context) {
        try {
            JSONObject jsonObj = new JSONObject(readFromJsonFile(context, VIDEO_FILE_NAME,false));
            JSONArray videosJsonArray = jsonObj.getJSONArray("videoFileDetails");
            ArrayList<Video> videos = new ArrayList<>(videosJsonArray.length());

            for(int i = 0; i < videosJsonArray.length(); i++) {
                Video vc = new Video(
                    videosJsonArray.getJSONObject(i).optInt("videoFileId",0),
                    videosJsonArray.getJSONObject(i).optString("name",""),
                    videosJsonArray.getJSONObject(i).optString("description",""),
                    videosJsonArray.getJSONObject(i).optString("filepath",""),
                    videosJsonArray.getJSONObject(i).optInt("questionId",0)
                );
                videos.add(vc);
            }
            return videos;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Adds a new video object to video file.
     * Maybe useful later when we want to create an user interface for adding new videos with questions.
     * @param context the application context
     * @param videoPath the path of the video to be added
     * @param questionId id of the question belonging to the video
     */
    public static void addVideoToJson(Context context, String videoPath, int questionId) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(readFromJsonFile(context, VIDEO_FILE_NAME,false));
            JSONArray videosJsonArray = jsonObj.getJSONArray("videoFileDetails");

            JSONObject videoObj = new JSONObject();
            videoObj.put("videoFileId", getNrOfVideos(context));
            videoObj.put("name", Utilities.getNameFromPath(videoPath));
            videoObj.put("description", "");
            videoObj.put("filepath", videoPath);
            videoObj.put("questionId", questionId);

            videosJsonArray.put(getNrOfVideos(context), videoObj);
            jsonObj.put("videoFileDetails", videosJsonArray);
            writeJsonToFile(jsonObj, context.getFilesDir()+"/"+VIDEO_FILE_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Extracts information of all worlds from worlds file and puts it in array list with world objects.
     * @param context the application context
     * @return array list with world objects with contain all needed information
     */
    public static ArrayList<World> readWorldsFromJson(Context context) {
        try {
            JSONObject jsonObj = new JSONObject(readFromJsonFile(context, WORLDS_FILE_NAME,false));
            JSONArray jsonArray = jsonObj.getJSONArray("worlds");
            ArrayList<World> worlds = new ArrayList<>(jsonArray.length());

            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject wayPointsJSON = new JSONObject(jsonArray.getJSONObject(i).getString("wayPoint"));
                ArrayList<ArrayList<String>> wayPointsArrayList = new ArrayList<>(wayPointsJSON.length());
                for (int j = 0; j < wayPointsJSON.length(); j++) {
                    ArrayList<String> wayPoint = new ArrayList<>();
                    wayPoint.add(wayPointsJSON.getJSONObject(String.valueOf(j)).getString("icon"));
                    wayPoint.add(wayPointsJSON.getJSONObject(String.valueOf(j)).getString("videoId"));
                    wayPointsArrayList.add(wayPoint);
                }
                World world = new World(
                    jsonArray.getJSONObject(i).optInt("worldId",0),
                    jsonArray.getJSONObject(i).optString("name",""),
                    jsonArray.getJSONObject(i).optString("map",""),
                    jsonArray.getJSONObject(i).optString("roadMap",""),
                    wayPointsArrayList
                );
                worlds.add(world);
            }
            return worlds;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Adds a new wayPoint containing a new video to a specified world in the world's json file.
     * @param context
     * @param worldName the name by which the world is identified
     * @param videoId the id of the video to be added as new wayPoint
     */
    public static void addWayPointToWorldJson(Context context, String worldName, int videoId) {
        try {
            JSONObject jsonObj = new JSONObject(readFromJsonFile(context, WORLDS_FILE_NAME, false));
            JSONArray worldsJsonArray = jsonObj.getJSONArray("worlds");
            for (int i = 0; i < worldsJsonArray.length(); i++) {
                if(worldsJsonArray.getJSONObject(i).optString("name").equals(worldName)) {
                    JSONObject world = worldsJsonArray.getJSONObject(i);
                    JSONObject wayPoints = new JSONObject(worldsJsonArray.getJSONObject(i).getString("wayPoint"));
                    JSONObject wayPoint = new JSONObject();
                    wayPoint.put("icon", "android.R.drawable.ic_menu_help");
                    wayPoint.put("videoId", videoId);
                    wayPoints.put(String.valueOf(wayPoints.length()), wayPoint);
                    world.put("wayPoint", wayPoints);
                }
            }
            jsonObj.put("worlds", worldsJsonArray);
            writeJsonToFile(jsonObj, context.getFilesDir()+"/"+WORLDS_FILE_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Extracts information of all question in question file and puts it in an array list with question objects.
     * @param context the application context
     * @return array list with question objects which contain all needed question data
     */
    public static ArrayList<Question> readQuestionsFromJson(Context context) {
        try {
            JSONObject jsonObj = new JSONObject(readFromJsonFile(context, QUESTION_FILE_NAME, false));
            JSONArray questionJsonArray = jsonObj.getJSONArray("questions");
            ArrayList<Question> questions = new ArrayList<>(questionJsonArray.length());

            for(int i = 0; i < questionJsonArray.length(); i++) {
                JSONObject answersJson = new JSONObject(questionJsonArray.getJSONObject(i).getString("answers"));
                ArrayList<String> answers = new ArrayList<>(answersJson.length());
                for (int j = 0; j < answersJson.length(); j++) {
                    answers.add(answersJson.getString("" + j));
                }
                Question qc = new Question(
                    questionJsonArray.getJSONObject(i).optInt("id",i),
                    questionJsonArray.getJSONObject(i).optString("question",""),
                    questionJsonArray.getJSONObject(i).optInt("right",0),
                    answers
                );
                questions.add(qc);
            }
            return questions;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Adds a new question to the question file.
     * Maybe useful later when we want to add function to add new questions via user interface.
     * @param context the application context
     * @param question question object of the new question
     */
    public static void addQuestionToJson(Context context, String question, ArrayList<String> answers, int rightAnswer) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(readFromJsonFile(context, QUESTION_FILE_NAME, false));
            JSONArray questionJsonArray = jsonObj.getJSONArray("questions");

            JSONObject questionObj = new JSONObject();
            questionObj.put("id", getNrOfQuestions(context));
            questionObj.put("question", question);
            JSONObject answerObj = new JSONObject();
            for(int i = 0; i < answers.size(); i++) {
                answerObj.put(String.valueOf(i), answers.get(i));
            }
            questionObj.put("answers", answerObj);
            questionObj.put("right", rightAnswer);

            questionJsonArray.put(getNrOfQuestions(context), questionObj);
            for(int i=0; i < questionJsonArray.length(); i++) {
                Log.d("question"+i, String.valueOf(questionJsonArray.get(i)));
            }
            jsonObj.put("questions", questionJsonArray);
            writeJsonToFile(jsonObj, context.getFilesDir()+"/"+QUESTION_FILE_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Helper method to read data from a json file, needs to be distinguished if profile or other data.
     * @param context the application context
     * @param filename name of the file from which json should be read
     * @param profileLoad if it's the profile that's being read or other data,
     *                    needs different kinds of reading, not sure why but it breaks if you try to generalize
     * @return a string with the file contents
     */
    private static String readFromJsonFile(Context context, String filename, boolean profileLoad) {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader = null;
        try {
            if(profileLoad) {
                reader = new BufferedReader(new FileReader(new File(context.getFilesDir(), PROFILE_FILE_NAME)));
            } else {
                try {
                    reader = new BufferedReader(new FileReader(new File(context.getFilesDir(), filename)));
                } catch (FileNotFoundException e) {
                    reader = new BufferedReader(new InputStreamReader(context.getAssets().open(filename), StandardCharsets.UTF_8));
                }
            }
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                stringBuilder.append(mLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.d("readJson", stringBuilder.toString());
        return stringBuilder.toString();
    }

    /**
     * Helper method to write a json object to a specified file.
     * @param jsonObj the json object in question
     * @param path the complete path of the file
     */
    private static void writeJsonToFile(JSONObject jsonObj, String path) {
        File file = new File(path);
        Log.d("filePath", path);
        Log.d("jsonToFile", jsonObj.toString());
        FileOutputStream outputStream;
        try {
            file.createNewFile();
            outputStream = new FileOutputStream(file, false);
            outputStream.write(jsonObj.toString().getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * A helper method to generate better readability of readProfileFromJson()
     * @param jsonObj the json object containing values for our new profile
     * @return a new user profile, either filled with values, or in case of exception a default one
     */
    private static UserProfile fillProfileWithValuesFromJsonFile(JSONObject jsonObj) {
        UserProfile userProfile = UserProfile.createDefaultProfile();
        ArrayList<Integer> questionIds = new ArrayList<>();
        try {
            for (String questionId : jsonObj.optString("questionIds", "").split(",")) {
                questionIds.add(Integer.parseInt(questionId));
            }
        } catch (NumberFormatException | NullPointerException e) {
            e.printStackTrace();
        }

        try {
            userProfile = new UserProfile(
                    jsonObj.optString("name", ""),
                    jsonObj.optInt("age", 0),
                    jsonObj.optInt("follower", 0),
                    jsonObj.optInt("stars", 0),
                    jsonObj.optString("color", ""),
                    questionIds,
                    jsonObj.optInt("localMap", 0)
            );
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return userProfile;
    }

    public static void addNewQuestion(Context context, String videoPath,
                                      String question, ArrayList<String> answers, int rightAnswer,
                                      String worldName) {
        int questionId = getNrOfQuestions(context);
        int videoId = getNrOfVideos(context);
        addQuestionToJson(context, question, answers, rightAnswer);
        addVideoToJson(context, videoPath, questionId);
        addWayPointToWorldJson(context, worldName, videoId);
    }

    /**
     * when creating new Question, the output of this method determines the current id
     * (because we start at id 0 it's always the number of already existing questions)
     * @param context the application context, thus the method can only be called from an Android class like Activity
     * @return either the number of questions in the question file or -1 if json exception occurred
     */
    public static int getNrOfQuestions(Context context) {
        try {
            JSONObject jsonObj = new JSONObject(readFromJsonFile(context, QUESTION_FILE_NAME, false));
            JSONArray questionJsonArray = jsonObj.getJSONArray("questions");
            return questionJsonArray.length();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * when creating new Video, the output of this method determines the current id
     * (because we start at id 0 it's always the number of already existing videos)
     * @param context the application context, thus the method can only be called from an Android class like Activity
     * @return either the number of videos in the video file or -1 if json exception occurred
     */
    public static int getNrOfVideos(Context context) {
        try {
            JSONObject jsonObj = new JSONObject(readFromJsonFile(context, VIDEO_FILE_NAME,false));
            JSONArray videosJsonArray = jsonObj.getJSONArray("videoFileDetails");
            return videosJsonArray.length();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
