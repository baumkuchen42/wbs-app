package de.hszg.wbs;

import java.util.ArrayList;

public class Question {
    private int id;
    private final String question;
    private final ArrayList<String> answers;
    private final int rightAnswer;

    Question(int id, String question, int rightAnswer, ArrayList<String> answers) {
        this.id = id;
        this.question = question;
        this.rightAnswer = rightAnswer;
        this.answers = answers;
    }

    public int getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public int getNumberOfAnswers() {
        return answers.size();
    }

    public int getRightAnswer() {
        return rightAnswer;
    }
}
