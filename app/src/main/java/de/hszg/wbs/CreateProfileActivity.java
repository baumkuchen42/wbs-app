package de.hszg.wbs;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CreateProfileActivity extends AppCompatActivity {

    private UserProfile wbsProfile;
    private EditText editName;
    private EditText editAge;
    private Button buttonColorYellow;
    private Button buttonColorRed;
    private Button buttonColorBlue;
    private Button buttonColorGreen;
    private Button buttonColorPurple;
    private Button buttonColorLightBlue;
    private Button buttonColorDarkGreen;
    private Button buttonColorOrange;

    /**
     * Contains standard activity launching stuff plus setting view variables,
     * loading wbs profile and filling in already existing values and
     * adding text change listeners.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.wbsProfile = JsonUtil.readProfileFromJson(this);
        Utilities.loadTheme(this, wbsProfile.getColor());
        setContentView(R.layout.activity_create_profile);
        findAndSetViewAttributes();

        prefillValues();

        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                wbsProfile.setName(s.toString());
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        editAge.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                try {
                    wbsProfile.setAge(Integer.parseInt(s.toString()));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
    }

    /**
     * Connected by android:onClick with nextButton.
     * Either displays warning that profile is still incomplete, or, if complete,
     * saves inputs as new wbs profile and launches FollowerChoiceActivity.
     * @param view is not used but transmitted by android:onClick
     */
    public void onNextButtonClicked(View view) {
        if(wbsProfile.getName() == "" || wbsProfile.getName().length() == 0 || wbsProfile.getColor() == "#FFFFFF") {
            Notifications.displayAlert(this, R.string.incompleteProfileTitle, R.string.incompleteProfileAlert);
        }
        else {
            wbsProfile.setDefaultProfile(false);
            Log.d("wbsProfile", wbsProfile.toString());
            JsonUtil.WBSProfileToJson(this, wbsProfile);
            Intent intent = new Intent(getApplicationContext(), FollowerChoiceActivity.class);
            if(getIntent().getBooleanExtra("editingExistingProfile", false)){
                intent.putExtra("editingExistingProfile", true);
            }
            startActivity(intent);
            finish();
        }
    }

    /**
     * If the wbs profile is not the default one, meaning, an existing profile gets edited,
     * a click on the back button goes back to the ShowProfileActivity.
     * Otherwise, the incomplete profile warning is displayed.
     */
    @Override
    public void onBackPressed(){
        if (!wbsProfile.isDefaultProfile()) {
            startActivity(new Intent(getApplicationContext(), ShowProfileActivity.class));
            finish();
        }
        else {
            Notifications.displayAlert(this, R.string.incompleteProfileTitle, R.string.incompleteProfileAlert);
        }
    }

    /**
     * Connected via android:onClick with all of the colorButtons.
     * Sets color in wbs profile and indicates, which color was chosen with "ok" on the resp. button.
     * @param button colorButton, gets transmitted with android:onClick
     */
    public void onColorButtonClicked(View button) {
        wbsProfile.setColor(button.getTag().toString());
        indicateChosenColorButton((Button) button);
    }

    /**
     * Sets the view attributes by finding the views by Id.
     */
    private void findAndSetViewAttributes() {
        editName = findViewById(R.id.CPA_edit_name);
        editAge = findViewById(R.id.CPA_edit_age);
        buttonColorYellow = findViewById(R.id.CPA_button_color_yellow);
        buttonColorRed = findViewById(R.id.CPA_button_color_red);
        buttonColorBlue = findViewById(R.id.CPA_button_color_blue);
        buttonColorGreen = findViewById(R.id.CPA_button_color_green);
        buttonColorPurple = findViewById(R.id.CPA_button_color_purple);
        buttonColorLightBlue = findViewById(R.id.CPA_button_color_lightblue);
        buttonColorDarkGreen = findViewById(R.id.CPA_button_color_darkgreen);
        buttonColorOrange = findViewById(R.id.CPA_button_color_orange);
    }

    /**
     * Pre-fills all inputs with values from already existing wbs profile.
     */
    @SuppressLint("ResourceType")
    private void prefillValues() {
        editName.setText("");
        editAge.setText("");

        if (wbsProfile != null) {
            editName.setText(wbsProfile.getName());
            editAge.setText(String.valueOf(wbsProfile.getAge()));
            String color = wbsProfile.getColor();
            if (color.equals(getResources().getString(R.color.yellow))) {
                indicateChosenColorButton(buttonColorYellow);
            }
            else if (color.equals(getResources().getString(R.color.red))) {
                indicateChosenColorButton(buttonColorRed);
            }
            else if (color.equals(getResources().getString(R.color.blue))) {
                indicateChosenColorButton(buttonColorBlue);
            }
            else if (color.equals(getResources().getString(R.color.green))) {
                indicateChosenColorButton(buttonColorGreen);
            }
            else if (color.equals(getResources().getString(R.color.purple))) {
                indicateChosenColorButton(buttonColorPurple);
            }
            else if (color.equals(getResources().getString(R.color.lightblue))) {
                indicateChosenColorButton(buttonColorLightBlue);
            }
            else if (color.equals(getResources().getString(R.color.darkgreen))) {
                indicateChosenColorButton(buttonColorDarkGreen);
            }
            else if (color.equals(getResources().getString(R.color.orange))) {
                indicateChosenColorButton(buttonColorOrange);
            }

            if (!wbsProfile.isDefaultProfile()) {
                TextView title = findViewById(R.id.CPA_text_head);
                title.setText(R.string.SPA_Edit);
            }
        }
    }

    /**
     * Indicates, which color button was clicked by displaying an "ok" on that button and
     * clearing all the other button labels before so that only one button has text displayed.
     * @param button the button to be highlighted
     */
    private void indicateChosenColorButton(Button button) {
        clearColorButtons();
        button.setText(R.string.ok);
    }

    /**
     * Clears all colorButton labels.
     */
    private void clearColorButtons() {
        buttonColorGreen.setText("");
        buttonColorBlue.setText("");
        buttonColorRed.setText("");
        buttonColorYellow.setText("");
        buttonColorDarkGreen.setText("");
        buttonColorLightBlue.setText("");
        buttonColorOrange.setText("");
        buttonColorPurple.setText("");
    }
}
