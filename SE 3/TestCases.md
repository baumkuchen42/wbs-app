# Use Case: Create Profile

## Variant 1: `CreateProfileActivity` is launch activity

**Actor:** Child together with parents

**Description:** Child opens the app and gets greeted with screen where they can create their own personalised user profile.

**Preconditions:** no user profile found

**Postconditions:** user profile is created and stored as json

**Basic flow:** 

1. Open app
2. *Splash screen*
3. *`CreateProfileActivity` is shown*
4. Fill in name `"Test"`
5. Tick gender `MÄNNLICH`
6. Fill in age `7`
7. Tick favourite color `YELLOW`
8. Click `WEITER`
9. *`FollowerChoiceActivity` is shown*
9. Choose Companion `UNICORN`
10. Click `WEITER`
11. *`MainActivity` is shown*
11. Go to hamburger menu and click on `Profil`
12. Check if 
    - Name = Test
    - Geschlecht = Männlich
    - Alter = 7
    - Lieblingsfarbe = `YELLOW`
    - Begleiter = `UNICORN`

## Variant 2: Edit Profile - `CreateProfileActivity` is opened via menu

**Actor:** Child together with parents

**Description:** Child wants to change something in their profile and opens the app menu for that.

**Preconditions:** user profile already exists

**Postconditions:** data of user profile is updated and stored as json

**Basic flow:** 

1. Open app
2. *Splash screen*
3. *`MainActivity` is shown* 
4. Click on hamburger menu
5. Click on menu entry `Profil`
6. Click on edit button
5. Change gender to `WEIBLICH`
8. Click `WEITER`
9. *`FollowerChoiceActivity` is shown*
9. Change Companion to `BEE`
10. Click `WEITER`
11. *`MainActivity` is shown*
11. Go to hamburger menu and click on `Profil`
12. Check if 
    - Name = Test
    - Geschlecht = Weiblich
    - Alter = 7
    - Lieblingsfarbe = `YELLOW`
    - Begleiter = `BEE`
 
 
# Use Case: Question

## Variant 1: Wrong Answer With Secound Try

**Actor:** Child that can read or child together with parents

**Description:** Child watches video, tries to answer question and gets it wrong.

**Preconditions:** existing user profile

**Postconditions:** ??

**Basic flow:** 

1. Open app
2. *Splash screen*
3. *`MainActivity` is shown*
4. Click on one of the question marks shown on the map
5. Watch video
6. Click `WEITER`
7. *`QuestionActivity` is shown*
7. Tick one of the two supposedly wrong answers
8. Click `WEITER`
9. *Alert for wrong question gets shown*
10. Click "NEUER VERSUCH"
11. *Alert is destroyed and questions are shown again*

## Variant 2: Wrong Answer Without Secound Try

**Actor:** Child that can read or child together with parents

**Description:** Child watches video, tries to answer question and gets it wrong.

**Preconditions:** existing user profile

**Postconditions:** ??

**Basic flow:** 

1. Open app
2. *Splash screen*
3. *`MainActivity` is shown*
4. Click on one of the question marks shown on the map
5. Watch video
6. Click `WEITER`
7. *`QuestionActivity` is shown*
7. Tick one of the two supposedly wrong answers
8. Click `WEITER`
9. *Alert for wrong question gets shown*
10. Click "SPÄTER NOCH EINMAL"
11. *`MainActivity` is shown*

## Variant 3: Right Answer

**Actor:** Child that can read or child together with parents

**Description:** Child watches video, tries to answer question and gets it right.

**Preconditions:** existing user profile, question (and possible duplicates of the question) didn't get answered yet

**Postconditions:** Belohnungen ++ 

**Basic flow:** 

1. Open app
2. *Splash screen*
3. *`MainActivity` is shown*
4. Click on one of the question marks shown on the map
5. Watch video
6. Click `WEITER`
7. *`QuestionActivity` is shown*
7. Tick the right answer
8. Click `WEITER`
9. *Alert for right question gets shown*
10. Click "WEITER..."
11. *`MainActivity` is shown*
12. *Question mark icon is replaced with star*
13. Click on hamburger menu
14. Open menu entry `Profil` 
15. Check if Belohnungs-Counter went up (bottom of the page, number next to star symbol)

# Use Case: Switch World 

## Variant 1: Evoking App Crash

**Actor:** Uta with own phone **(should be tested with other devices too)**

**Description:** Sometimes the app crashes when switching to next world

**Preconditions:** existing user profile, next world isn't in cache yet or anymore (happens when the world is opened for first time or when some time passed since the last opening)

**Postconditions:** after crash, next world is active

**Basic flow:** 

1. Open app
2. *Splash screen*
3. *`MainActivity` is shown*
4. Click arrow on the right bottom of the app
5. *App crashes*
6. Open app again
7. *Splash screen*
8. *`MainActivity` with incremented world id is shown, App is now one world further*

## Variant 2: One World Forward

**Actor:** Child that can read or child together with parents

**Description:** Child switches to the next world (either because they're finished or because they're bored)

**Preconditions:** existing user profile, not in the last world

**Postconditions:** next world is active

**Basic flow:** 

1. Open app
2. *Splash screen*
3. *`MainActivity` is shown*
4. Click arrow on the right bottom of the app
5. *Transition `EnterWorldActivity` is shown*
6. *`MainActivity` with incremented world id is shown, App is now one world further*

## Variant 3: One World Backward

**Actor:** Child that can read or child together with parents

**Description:** Child switches to the previous world

**Preconditions:** existing user profile, not in first world

**Postconditions:** previous world is active

**Basic flow:** 

1. Open app
2. *Splash screen*
3. *`MainActivity` is shown*
4. Click arrow on the left bottom of the app
5. *Transition `EnterWorldActivity` is shown*
6. *`MainActivity` with decremented world id is shown, App is now one world back*