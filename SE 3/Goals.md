1. Implementiert die Software die in der Analyse beschriebenen Use Cases? (Nutzen Sie zunächst fachliche Tests, um dies festzustellen. Nutzen Sie die Dokumentation, um die Use Cases zu identifizieren. 3 fachliche Tests per Use Case sind nicht unüblich. Konzipieren Sie ein mögliches technologisches Vorgehen, um die Tests durchzuführen. Unmittelbar danach soll dieses Vorgehen implementiert und die Tests ausgeführt werden.)

2. Enthalten die vorhandenen implementierten Use Cases noch Fehler (siehe 1.)?

3. Sind die vorhandenen implementierten Use Cases zwar fachlich fehlerfrei, sind aber kein Clean Code? (Konzipieren Sie ein mögliches Vorgehen für ein Refactoring durch Nutzung von Regressionstests zur Sicherstellung der Funktionalität. Größtenteils auch siehe 1.)
Dauer des Vortrages ca. 10-15min. Keine Folien nötig. Formatieren Sie das Konzept sauber, dann können wir es für die Präsentation nutzen und durch scrollen.
 
Ziel des Semesters:
- Software besteht fachliche Tests (basierend auf Use Cases).
- Software genügt den Ansprüchen von Clean Code