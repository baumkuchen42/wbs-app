# Refactoring
### getisProfile
- checks whether the Profile has all needed values
- the current name is not very informative or even misleading
- maybe we can get rid of this by changing the structure of createProfileActivity
    - instead of creating an empty UserProfileClass object at the beginning we could just collect the needed values and create the Profile with all needed values via the constructor


### createProfileActivity
- change variable 'gender' to use the enum UserProfileClass.Gender instead of String
- consider only creating
- correct spelling mistakes 
	- meine Name
	- meine Geschlecht
- consider changing the way colors are represented internally (maybe useful)
- change background to white

# TODO
### Proper Videos & Questions
- fill with animated videos 
	- are there any available ressources or people who could help?
	    - yes, students of prof. Prosetzky (Sonja Jähnig and Martin Stekler) will shoot some videos
- think of useful scenarios
    - will propably be handled for. we will use the topics from aforementioned videos
- currently the questions and answers are both in textform. our app also targets young children that can't read yet
    - idea one: record audiofiles for the questions and use pictograms for the answer choices
    - idea two: have the questions at the end of the video, also with pictograms
        - discuss with Sonja Jähnig and Martin Stekler

### Effect of profile settings
- apparently age, favourite color, etc. don't have any effect on appearance or kind of questions yet
    - users should enter their age by giving their birth date. that enables us to keep the applicable age-group up to date 
    and allows for a nifty extra feature where the app could congratulate the user the first time it is opened on their birthday
        - [possible implementation](https://stackoverflow.com/questions/4700285/android-how-to-set-an-alarm-to-a-specific-date)

### TODOS from last group
- **first of all:** find out what the following features are supposed to be
- in the app as yet-to-be-implemented future features are:
	- ToDo-Liste 
	    - soll mit Handlungstipps für Notfallsituationen gefüllt werden, die die Eltern dort eintragen können
	    - vllt. lieber mit Klick auf den Begleiter aufrufen, als durch merkwürdigen Bezeichner "ToDo-Liste"
	- Einstellungen
	- Shop (see first point under *Improvements*)

### Tests
- mainly GUI logic --> usability tests
- write down some workflows to test each time we change sth. --> see`TestCases.md`
- test "end" product with the kids
- write Unit tests for`JsonUtil`

# Improvements
### Shop
- should there really be a shop in a children's app, especially when the children have the huge problem of being a bit to naive for the world?

### Path picture
- the picture of the path is blurry 
	- --> find a better one

### child-friendlyness
- more icons instead of text
    - see section TODO: Proper Videos & Questions
- lets design a nicer app icon which is more appealing to kids
- include companion more

### Gender specific settings
- what purpose does the inquiry of gender serve? (Prinzip der Datensparsamkeit)

### GUI for adding questions
- makes it possible for parents to add questions for their child to costumize the experience
- until now this only works via json
- **Problem:** shouldn't be accessible for the child, should it?

### Background music
- in profile possibility to choose music file which gets played in a loop as background music